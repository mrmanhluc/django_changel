
### Overview

![alt text](./img/01.png)

Websocket về cơ bản cũng như HTTP, được connect bằng ws.
Ví dụ thay vì http://localhost thì sẽ connect bằng ws://localhost

### I. Setup

Step 1. Setup python3.6

Step 2. Install Django + channels
```python
pip3.6 install -r requirements
```

Step 3. Install Redis
```python
yum install redis
```

### II. Create Project

#### 1. Create Basic App
Tạo app basic.



Step 1. Tạo porject name mysite
```python
django-admin startproject mysite
```

Kết quả : 
```python
mysite/
    manage.py
    mysite/
        __init__.py
        settings.py
        urls.py
        wsgi.py
```

Step 2. Create Chat App
```python
python3 manage.py startapp chat
```

Kết quả 
```python
chat/
    __init__.py
    admin.py
    apps.py
    migrations/
        __init__.py
    models.py
    tests.py
    views.py
```

Step 3 : Edit mysite/settings.py
```python
# Allow access from all host
ALLOWED_HOSTS = ['*']
...

INSTALLED_APPS = [
    'chat',
    'channels',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

Step 4 : Tạo PageIndex 

chat/templates/chat/index.html
```python
<!-- chat/templates/chat/index.html -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Chat Rooms</title>
</head>
<body>
    What chat room would you like to enter?<br/>
    <input id="room-name-input" type="text" size="100"/><br/>
    <input id="room-name-submit" type="button" value="Enter"/>

    <script>
        document.querySelector('#room-name-input').focus();
        document.querySelector('#room-name-input').onkeyup = function(e) {
            if (e.keyCode === 13) {  // enter, return
                document.querySelector('#room-name-submit').click();
            }
        };

        document.querySelector('#room-name-submit').onclick = function(e) {
            var roomName = document.querySelector('#room-name-input').value;
            window.location.pathname = '/chat/' + roomName + '/';
        };
    </script>
</body>
</html>

```

Step 5. Edit views.py
```python
# chat/views.py
from django.shortcuts import render

def index(request):
    return render(request, 'chat/index.html', {})
```

Step 6. Tạo file urls.py tại /chat/urls.py
```python
# chat/urls.py
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
]

```

Step 7. Edit file urls.py tại /mysite/urls.py
````python
# mysite/urls.py
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^chat/', include('chat.urls')),
    url(r'^admin/', admin.site.urls),
]
````


Test : 
```python
Run :
python3.6 manage.py runserver 192.168.11.21:80
```

#### 2. Kết hợp Channels với Django

Tạo một số thành phần sau : 
* /mysite/routing.py : Giống urls, có nhiệm vụ điều phối task. Cho biết function nào sẽ run khi nhận message
* /mysite/

Step 1. Tạo routing.py
```python
# mysite/routing.py
from channels.routing import ProtocolTypeRouter

application = ProtocolTypeRouter({
    # (http->django views is added by default)
})
```

Step 2. Edit setting.py
```python
...
INSTALLED_APPS = [
    'channels',
    'chat',
    'django.contrib.admin',
    ...
]
...

ASGI_APPLICATION = 'mysite.routing.application'
```

#### III. Create Chatting Room

Step 1 : Tạo chat/templates/chat/room.html

```html
<!-- chat/templates/chat/room.html -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Chat Room</title>
</head>
<body>
    <textarea id="chat-log" cols="100" rows="20"></textarea><br/>
    <input id="chat-message-input" type="text" size="100"/><br/>
    <input id="chat-message-submit" type="button" value="Send"/>
</body>
<script>
    var roomName = {{ room_name_json }};

    var chatSocket = new WebSocket(
        'ws://' + window.location.host +
        '/ws/chat/' + roomName + '/');

    chatSocket.onmessage = function(e) {
        var data = JSON.parse(e.data);
        var message = data['message'];
        document.querySelector('#chat-log').value += (message + '\n');
    };

    chatSocket.onclose = function(e) {
        console.error('Chat socket closed unexpectedly');
    };

    document.querySelector('#chat-message-input').focus();
    document.querySelector('#chat-message-input').onkeyup = function(e) {
        if (e.keyCode === 13) {  // enter, return
            document.querySelector('#chat-message-submit').click();
        }
    };

    document.querySelector('#chat-message-submit').onclick = function(e) {
        var messageInputDom = document.querySelector('#chat-message-input');
        var message = messageInputDom.value;
        chatSocket.send(JSON.stringify({
            'message': message
        }));

        messageInputDom.value = '';
    };
</script>
</html>
```

Step 2 : Edit file chat/views.py

Mục đích : Tạo room chat.
```python
# chat/views.py
from django.shortcuts import render
from django.utils.safestring import mark_safe
import json

def index(request):
    return render(request, 'chat/index.html', {})

def room(request, room_name):
    return render(request, 'chat/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name))
    })
```

Step 3 : Edit /chat/urls.py
```python
# chat/urls.py
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<room_name>[^/]+)/$', views.room, name='room'),
]
```


Step 4 : Run server

Test : Khi nhập hello, không có sự kiện nào xảy ra.
Nguyên nhân do chưa tạo Consumer để accept connection.

```python
python3 manage.py runserver 192.168.11.21:80
```


-----
Tham khảo : 
- https://channels.readthedocs.io/en/latest/tutorial/part_2.html